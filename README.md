# Task PingPong
Task PingPong is a collaborative tool for managing repetitive tasks.
A great example of a task which repeats and requires collaboration would be 
feeding the cat. Is mr kitty actually hungry? Has he been fed already? 
These types of hard hitting questions will be solved by using this app.

# Screenshots
Welcome to the homepage. This is a list of all the tasks you've authored.

![home](screenshots/home.png)

## Creating a Task
Upon clicking the 'create' icon in the top right, you can fill out a
form to author your first task.  
Repetition is optional, and when it's selected your task will be 
opened daily at the specified time.

![Create Task](screenshots/create-task.png)

## Finishing a Task
Finishing a task is easy, just press the tick icon.  
To open a task, tap the 'X' icon.

![Finish Task](screenshots/finish-task.png)

## Sharing a Task
To share a task, first open up the options menu by pressing the three dot icon on your task

![Task Menu](screenshots/task-menu.png)

Then you can search and add people to be collaborators.

![Share Task](screenshots/share-task.png)

## Collaborating
You can find the collaborating page from the side menu

![Collaborating](screenshots/collaborating.png)

Here you'll find a page similar to the homepage, 
except its listing tasks that have been shared 
with you.

## Unfollowing a Task
From the collaborating page, you can unfollow any task by first pressing the three dot icon, then the unfollow button.

![Task Unfollow](screenshots/unfollow-task.png)

# Running
To run this app, you need to [create a Firebase application](https://firebase.google.com/docs), 
then copy the [Firebase config object](https://firebase.google.com/docs/web/setup?authuser=0#config-object) 
into environment.ts.
```shell
mv src/environment/environment.example.ts 
src/environment/environment.ts
cp src/environment/environment.ts src/environment/environment.prod.ts
```

## Prerequisites
This application was developed using [Ionic](https://ionicframework.com/) so make sure you have it [installed](https://ionicframework.com/getting-started).

## Serving in the Browser
```
ionic serve
```

## Running as a Mobile App
For android
```
ionic cordova run android
```

For ios
```
ionic cordova run ios
```

# Credits

The project avatar and app icon/splash is from [Font Awesome](https://fontawesome.com/license).
