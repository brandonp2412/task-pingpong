import {TestBed} from '@angular/core/testing';

import {AuthService} from './auth.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {Platform, LoadingController} from '@ionic/angular';
import {UserService} from './user.service';
import {ToastService} from './toast.service';
import {Router} from '@angular/router';
import {of} from 'rxjs';

fdescribe('AuthService', () => {
  let afAuthSpy,
    afSignOutSpy,
    gplusLogoutSpy,
    platformSpy,
    presentLoadingSpy,
    createUserSpy,
    showToastSpy,
    navigateSpy;
  let service: AuthService;

  beforeEach(() => {
    afSignOutSpy = jasmine.createSpyObj('AngularFireAuth', {
      auth: {
        signOut: Promise.resolve(),
      },
      user: of({uid: 'fake-uid'}),
    });
    gplusLogoutSpy = jasmine.createSpyObj('GooglePlus', ['logout']);
    platformSpy = jasmine.createSpyObj('Platform', ['is']);
    presentLoadingSpy = jasmine.createSpyObj('LoadingController', ['present']);
    createUserSpy = jasmine.createSpyObj('UserService', ['createUser']);
    showToastSpy = jasmine.createSpyObj('ToastService', ['show']);
    navigateSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      providers: [
        {provide: AngularFireAuth, useValue: afSignOutSpy},
        {provide: GooglePlus, useValue: gplusLogoutSpy},
        {provide: Platform, useValue: platformSpy},
        {provide: LoadingController, useValue: presentLoadingSpy},
        {provide: UserService, useValue: createUserSpy},
        {provide: ToastService, useValue: showToastSpy},
        {provide: Router, useValue: navigateSpy},
      ],
    });
  });

  it('should be created', () => {
    service = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it('should sign out', () => {
    service.logout();
    expect(afSignOutSpy).toHaveBeenCalled();
  });
});
