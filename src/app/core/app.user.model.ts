export interface AppUser {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  friends?: string[];
  invites?: string[];
}
