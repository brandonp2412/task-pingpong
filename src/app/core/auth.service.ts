import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {Platform, LoadingController} from '@ionic/angular';
import {environment} from 'src/environments/environment';
import {UserService} from './user.service';
import {auth, User} from 'firebase/app';
import {ToastService} from './toast.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public user: Observable<User>;

  constructor(
    private afAuth: AngularFireAuth,
    private gplus: GooglePlus,
    private platform: Platform,
    private loadingController: LoadingController,
    private userService: UserService,
    private toastService: ToastService,
    private router: Router
  ) {
    this.user = afAuth.user;
  }

  logout() {
    this.afAuth.auth.signOut();
    if (this.platform.is('cordova')) {
      this.gplus.logout();
    }
    this.router.navigate(['/login']);
  }

  async googleLogin() {
    this.platform.is('cordova')
      ? await this.nativeGoogleLogin()
      : await this.webGoogleLogin();
    this.router.navigate(['/home']);
  }

  async nativeGoogleLogin() {
    const loading = await this.showLoading();
    try {
      const gplusUser = await this.gplus.login({
        webClientId: environment.webClientId,
        offline: true,
        scopes: 'profile email',
      });
      const credential = await this.afAuth.auth.signInWithCredential(
        auth.GoogleAuthProvider.credential(gplusUser.idToken),
      );
      this.userService.createUser(credential.user).subscribe(() => {
        this.toastService.show('Successfully logged in.');
      });
    } finally {
      await loading.dismiss();
    }
  }

  async webGoogleLogin() {
    const loading = await this.showLoading();
    try {
      const credential = await this.afAuth.auth.signInWithPopup(
        new auth.GoogleAuthProvider(),
      );
      this.userService.createUser(credential.user).subscribe(() => {
        this.toastService.show('Successfully logged in.');
      });
    } finally {
      loading.dismiss();
    }
  }

  async showLoading() {
    const loading = await this.loadingController.create({
      message: 'Logging you in...',
    });
    await loading.present();
    return loading;
  }
}
