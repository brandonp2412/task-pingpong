import {Injectable} from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import {AppUser} from './app.user.model';
import {mergeMap} from 'rxjs/operators';
import { User } from 'firebase/app';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private users: AngularFirestoreCollection<AppUser>;

  private readonly resourceUrl = '/users';

  constructor(private db: AngularFirestore) {
    this.users = this.db.collection<AppUser>(this.resourceUrl);
  }

  public query() {
    return this.users.valueChanges();
  }

  public createUser(user: User) {
    return this.db
      .doc<AppUser>(this.resourceUrl + `/${user.uid}`)
      .get()
      .pipe(
        mergeMap(currentUser => {
          if (currentUser.exists) return of();
          return this.db.doc<AppUser>(`${this.resourceUrl}/${user.uid}`).set({
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
          });
        }),
      );
  }

}
