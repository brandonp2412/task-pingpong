import {Component, OnInit} from '@angular/core';
import {AuthService} from '../core/auth.service';
import {TaskService} from '../tasks/task.service';
import {Observable} from 'rxjs';
import {
  ActionSheetController,
} from '@ionic/angular';
import {ToastService} from '../core/toast.service';
import {Router} from '@angular/router';
import {Task} from '../tasks/task.model';
import {take} from 'rxjs/operators';
import { TaskRepeaterService } from '../tasks/task-repeater.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public myTasks: Observable<Task[]>;
  public collaborating: Observable<Task[]>;

  constructor(
    public auth: AuthService,
    private taskService: TaskService,
    private actionSheetController: ActionSheetController,
    private toastService: ToastService,
    private router: Router,
    private taskRepeater: TaskRepeaterService
  ) {}

  ngOnInit(): void {
    this.myTasks = this.taskService.getMyTasks();
    this.taskRepeater.scheduleTasks();
  }

  reloadPage(event: any) {
    this.ngOnInit();
    this.myTasks.pipe(take(1)).subscribe(() => event.target.complete());
  }

  async options(task: Task) {
    const actionSheet = await this.actionSheetController.create({
      header: task.title,
      buttons: [
        {
          text: 'Delete',
          role: 'destructive',
          icon: 'trash',
          handler: () => {
            this.taskService.delete(task.id);
            this.toastService.show('Task has been deleted.');
          },
        },
        {
          text: 'Edit',
          icon: 'create',
          handler: () => {
            this.router.navigate([`/task/${task.id}`]);
          },
        },
        {
          text: 'Collaborators',
          icon: 'person-add',
          handler: () => this.router.navigate([`/task/${task.id}/share`]),
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
        },
      ],
    });
    await actionSheet.present();
  }

  deleteTask(id: string) {
    this.taskService.delete(id);
  }

  async finishTask(task: Task) {
    this.taskService.update({
      id: task.id,
      done: true,
    });
    await this.toastService.show('Task has been finished.');
  }

  async openTask(task: Task) {
    this.taskService.update({
      id: task.id,
      done: false,
    });
    await this.toastService.show('Task has been opened.');
  }
}
