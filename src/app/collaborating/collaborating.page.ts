import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Task} from '../tasks/task.model';
import {AuthService} from '../core/auth.service';
import {TaskService} from '../tasks/task.service';
import {ActionSheetController} from '@ionic/angular';
import {ToastService} from '../core/toast.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-collaborating',
  templateUrl: './collaborating.page.html',
  styleUrls: ['./collaborating.page.scss'],
})
export class CollaboratingPage implements OnInit {
  public tasks: Observable<Task[]>;

  constructor(
    public auth: AuthService,
    private taskService: TaskService,
    private actionSheetController: ActionSheetController,
    private toastService: ToastService,
  ) {}

  ngOnInit(): void {
    this.tasks = this.taskService.getCollaborating();
  }

  reloadPage(event: any) {
    this.ngOnInit();
    this.tasks.pipe(take(1)).subscribe(() => event.target.complete());
  }

  async options(task: Task) {
    const actionSheet = await this.actionSheetController.create({
      header: task.title,
      buttons: [
        {
          text: 'Unfollow',
          role: 'destructive',
          icon: 'remove',
          handler: () => {
            this.unfollowTask(task.id);
          },
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
        },
      ],
    });
    await actionSheet.present();
  }

  unfollowTask(id: string) {
    this.taskService
      .unfollow(id)
      .subscribe(() => this.toastService.show('Task has been unfollowed.'));
  }

  async finishTask(task: Task) {
    this.taskService.update({
      id: task.id,
      done: true,
    });
    await this.toastService.show('Task has been finished.');
  }

  async openTask(task: Task) {
    this.taskService.update({
      id: task.id,
      done: false,
    });
    await this.toastService.show('Task has been opened.');
  }
}
