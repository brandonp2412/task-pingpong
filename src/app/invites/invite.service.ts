import {Injectable} from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import {Invite} from './invite.model';

@Injectable({
  providedIn: 'root',
})
export class InviteService {
  private resourceUrl = 'invites';
  private incomingInvites: AngularFirestoreCollection<Invite>;

  constructor(private db: AngularFirestore) {}
}
