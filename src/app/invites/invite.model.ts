export interface Invite {
  fromUid: string;
  toUid: string;
}
