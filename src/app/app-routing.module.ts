import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {
  AngularFireAuthGuard,
  hasCustomClaim,
  redirectUnauthorizedTo,
  redirectLoggedInTo,
  canActivate,
} from '@angular/fire/auth-guard';

const redirectUnauthorizedToLogin = redirectUnauthorizedTo(['login']);
const redirectLoggedInToHome = redirectLoggedInTo(['home']);

const routes: Routes = [
  {
    path: 'login',
    loadChildren: './login/login.module#LoginPageModule',
    ...canActivate(redirectLoggedInToHome),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule',
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'task/:id',
    loadChildren: './tasks/task-edit/task-edit.module#TaskEditPageModule',
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'task/:id/share',
    loadChildren: './tasks/task-share/task-share.module#TaskSharePageModule',
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'collaborating',
    loadChildren:
      './collaborating/collaborating.module#CollaboratingPageModule',
    ...canActivate(redirectUnauthorizedToLogin),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
