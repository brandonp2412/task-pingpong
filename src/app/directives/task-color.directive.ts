import {
  Directive,
  Input,
  ElementRef,
  Renderer2,
  SimpleChanges,
  OnChanges,
} from '@angular/core';

@Directive({
  selector: '[appTaskColor]',
})
export class TaskColorDirective implements OnChanges {
  @Input('appTaskColor') done: boolean;

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnChanges(_changes: SimpleChanges): void {
    this.done
      ? this.renderer.setAttribute(this.el.nativeElement, 'color', 'medium')
      : this.renderer.setAttribute(this.el.nativeElement, 'color', '');
  }
}
