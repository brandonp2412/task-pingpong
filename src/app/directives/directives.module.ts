import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskColorDirective} from './task-color.directive';

@NgModule({
  declarations: [TaskColorDirective],
  imports: [CommonModule],
  exports: [TaskColorDirective],
})
export class DirectivesModule {}
