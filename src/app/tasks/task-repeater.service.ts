import {Injectable, OnDestroy} from '@angular/core';
import {TaskService} from './task.service';
import {take} from 'rxjs/operators';
import {Task} from './task.model';

@Injectable({
  providedIn: 'root',
})
export class TaskRepeaterService implements OnDestroy {
  public taskScheduler: NodeJS.Timeout;

  constructor(private taskService: TaskService) {}

  ngOnDestroy() {
    clearInterval(this.taskScheduler);
  }

  /**
   * Schedules task repetition every minute.
   */
  public scheduleTasks(): void {
    clearInterval(this.taskScheduler);
    this.taskScheduler = setInterval(() => this.repeatTasks(), 1 * 1000 * 60);
  }

  /**
   * Opens tasks which are supposed to be repeated at the current minute:hour.
   */
  private repeatTasks(): void {
    this.taskService
      .getMyTasks()
      .pipe(take(1))
      .subscribe(tasks => {
        const now = new Date();
        const nowHours = now.getHours();
        const nowMinutes = now.getMinutes();
        tasks.forEach(task => {
          if (!this.taskShouldRepeat(nowHours, nowMinutes, task)) return;
          this.taskService.update({id: task.id, done: false});
        });
      });
  }

  /**
   * Gets whether or not a task should be repeated.
   *
   * @param nowHours The current dates hours
   * @param nowMinutes The current dates minutes
   * @param task The task to conditionally open
   */
  private taskShouldRepeat(
    nowHours: number,
    nowMinutes: number,
    task: Task,
  ): boolean {
    if (!task.repeat) return false;
    const taskRepeatSplit = task.repeat.split(':');
    const taskHours = Number(taskRepeatSplit[0]);
    const taskMinutes = Number(taskRepeatSplit[1]);
    return taskHours === nowHours && taskMinutes === nowMinutes;
  }
}
