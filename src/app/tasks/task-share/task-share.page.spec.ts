import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskSharePage } from './task-share.page';

describe('TaskSharePage', () => {
  let component: TaskSharePage;
  let fixture: ComponentFixture<TaskSharePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskSharePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskSharePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
