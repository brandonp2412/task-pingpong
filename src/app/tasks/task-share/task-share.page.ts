import {Component, OnInit, OnDestroy} from '@angular/core';
import {Task} from '../task.model';
import {TaskService} from '../task.service';
import {ActivatedRoute} from '@angular/router';
import {Observable, BehaviorSubject, combineLatest, Subscription} from 'rxjs';
import {AppUser} from 'src/app/core/app.user.model';
import {UserService} from 'src/app/core/user.service';
import {map, take} from 'rxjs/operators';
import {ToastService} from 'src/app/core/toast.service';
import {AuthService} from 'src/app/core/auth.service';

@Component({
  selector: 'app-task-share',
  templateUrl: './task-share.page.html',
  styleUrls: ['./task-share.page.scss'],
})
export class TaskSharePage implements OnInit, OnDestroy {
  public task: Task;
  public users: Observable<AppUser[]>;
  public search: string;

  private searchSubject: BehaviorSubject<string | null>;
  private taskSubscription: Subscription;

  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private userService: UserService,
    private authService: AuthService,
  ) {}

  ngOnInit() {
    this.setTask();
    this.searchSubject = new BehaviorSubject(null);
    this.users = combineLatest(
      this.userService.query(),
      this.searchSubject,
      this.authService.user,
    ).pipe(
      map(([users, search, authUser]) =>
        users
          .filter(
            user =>
              !search ||
              user.email.match(search) ||
              user.displayName.match(search),
          )
          .filter(user => user.uid !== authUser.uid),
      ),
    );
  }

  private setTask() {
    this.taskSubscription = this.taskService
      .find(this.route.snapshot.params['id'])
      .subscribe(task => {
        this.task = task;
      });
  }

  public shared(uid: string) {
    if (!this.task.shared) return false;
    return this.task.shared.includes(uid);
  }

  async share(uid: string) {
    const shared = this.task.shared || [];
    shared.push(uid);
    await this.taskService.update({id: this.task.id, shared: shared});
    await this.toastService.show('Added collaborator.');
    this.setTask();
  }

  async unshare(uid: string) {
    let shared = this.task.shared || [];
    shared = shared.filter(share => share !== uid);
    await this.taskService.update({id: this.task.id, shared: shared});
    await this.toastService.show(`Removed collaborator.`);
    this.setTask();
  }

  public onSearch() {
    this.searchSubject.next(this.search);
  }

  ngOnDestroy() {
    this.taskSubscription.unsubscribe();
  }
}
