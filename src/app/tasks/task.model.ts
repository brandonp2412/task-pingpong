export interface Task {
  id?: string;
  uid?: string;
  title?: string;
  done?: boolean;

  /**
   * A time string in 24-hour format. E.g. "13:49"
   */
  repeat?: string;

  /**
   * The UIDs of firebase users this Task is shared with.
   */
  shared?: string[];
}
