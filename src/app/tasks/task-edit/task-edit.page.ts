import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskService} from '../task.service';
import {ToastService} from '../../core/toast.service';
import {AuthService} from '../../core/auth.service';
import {Task} from '../task.model';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.page.html',
  styleUrls: ['./task-edit.page.scss'],
})
export class TaskEditPage implements OnInit {
  task: Task = {};

  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private toastService: ToastService,
    private router: Router,
    private auth: AuthService,
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    if (id === 'add') return (this.task = {done: false});
    this.taskService.find(id).subscribe(task => (this.task = task));
  }

  submit() {
    this.task.id ? this.update() : this.create();
  }

  create() {
    this.auth.user.pipe(take(1)).subscribe(user => {
      this.taskService.create({uid: user.uid, ...this.task});
      this.toastService.show('Created a Task.');
      this.router.navigate(['/home']);
    });
  }

  update() {
    this.taskService.update(this.task);
    this.toastService.show('Updated a Task.');
    this.router.navigate(['/home']);
  }
}
