import { TestBed } from '@angular/core/testing';

import { TaskRepeaterService } from './task-repeater.service';

describe('TaskRepeaterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaskRepeaterService = TestBed.get(TaskRepeaterService);
    expect(service).toBeTruthy();
  });
});
