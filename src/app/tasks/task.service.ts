import {Injectable} from '@angular/core';
import {
  AngularFirestoreCollection,
  AngularFirestore,
} from '@angular/fire/firestore';
import {Observable, concat, of} from 'rxjs';
import {map, mergeMap, take, filter} from 'rxjs/operators';
import {Task} from './task.model';
import {AuthService} from '../core/auth.service';
import {User} from 'firebase/app';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  private resourceUrl = 'tasks';
  private tasks: AngularFirestoreCollection<Task>;
  private user: Observable<User>;

  constructor(private db: AngularFirestore, authService: AuthService) {
    this.tasks = db.collection<Task>(this.resourceUrl);
    this.user = authService.user;
  }

  create(task: Task): void {
    this.tasks.add(task);
  }

  update(task: Task) {
    const taskDoc = this.db.doc<Task>(`${this.resourceUrl}/${task.id}`);
    return taskDoc.update(task);
  }

  delete(id: string): void {
    const taskDoc = this.db.doc<Task>(`${this.resourceUrl}/${id}`);
    taskDoc.delete();
  }

  /**
   * Removes the user from the shared attribute for a Task.
   * 
   * @param id ID of the target Task
   */
  unfollow(id: string): Observable<void> {
    return this.user.pipe(
      take(1),
      mergeMap(user =>
        this.find(id).pipe(
          mergeMap(task => {
            const shared = task.shared.filter(uid => uid !== user.uid);
            return this.update({id: id, shared: shared});
          }),
        ),
      ),
    );
  }

  find(id: string): Observable<Task> {
    return this.db
      .doc<Task>(`${this.resourceUrl}/${id}`)
      .snapshotChanges()
      .pipe(
        take(1),
        map(snapshot => ({
          id: snapshot.payload.id,
          ...snapshot.payload.data(),
        })),
      );
  }

  getAll(): Observable<Task[]> {
    return concat(this.getMyTasks(), this.getCollaborating());
  }

  getMyTasks(): Observable<Task[]> {
    return this.user.pipe(
      filter(user => !!user),
      mergeMap(user => {
        return this.db
          .collection<Task>(this.resourceUrl, ref =>
            ref.where('uid', '==', user.uid),
          )
          .valueChanges({idField: 'id'});
      }),
    );
  }

  getCollaborating(): Observable<Task[]> {
    return this.user.pipe(
      filter(user => !!user),
      mergeMap(authUser => {
        return this.db
          .collection<Task>(this.resourceUrl, ref =>
            ref.where('shared', 'array-contains', authUser.uid),
          )
          .valueChanges({idField: 'id'});
      }),
    );
  }
}
