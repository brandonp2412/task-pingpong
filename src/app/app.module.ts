import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AngularFireModule} from '@angular/fire';
import {environment} from 'src/environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(
      environment.firebaseConfig,
      'task-pingpong',
    ),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    AngularFireAuthGuardModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GooglePlus,
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    BackgroundMode
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
